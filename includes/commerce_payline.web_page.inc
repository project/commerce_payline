<?php

/**
 * Implements CALLBACK_commerce_payment_method_settings_form().
 *
 * @param null $settings
 *   The payment method settings.
 * @return array
 *   The settings form.
 */
function commerce_payline_web_page_settings_form($settings = NULL) {
  $form = array();

  $form['integration_mode'] = array(
    '#type' => 'select',
    '#title' => t('Integration mode'),
    '#default_value' => isset($settings['integration_mode'])
      ? $settings['integration_mode']
      : NULL,
    '#options' => array(
      //'in_shop' => t('In Shop'),
      //'lightbox' => t('Lightbox'),
      //'shortcut' => t('Shortcut'),
      'redirection' => t('Redirection'),
      //'redirection_version_2' => t('Redirection version 2'),
      //'iframe' => t('iFrame'),
      //'tpev' => t('TPEV'),
    ),
    '#required' => TRUE,
  );

  $form['environment'] = array(
    '#type' => 'select',
    '#title' => t('Environment'),
    '#default_value' => isset($settings['environment'])
      ? $settings['environment']
      : NULL,
    '#options' => array(
      'HOMO' => t('Test'),
      'PROD' => t('Production'),
    ),
    '#required' => TRUE,
  );

  $form['merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => isset($settings['merchant_id'])
      ? $settings['merchant_id']
      : NULL,
    '#required' => TRUE,
  );

  $form['access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Key'),
    '#default_value' => isset($settings['access_key'])
      ? $settings['access_key']
      : NULL,
    '#required' => TRUE,
  );

  $form['proxy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Proxy'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['proxy']['host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => isset($settings['host'])
      ? $settings['host']
      : NULL,
  );

  $form['proxy']['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => isset($settings['port'])
      ? $settings['port']
      : NULL,
  );

  $form['proxy']['login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#default_value' => isset($settings['login'])
      ? $settings['login']
      : NULL,
  );

  $form['proxy']['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => isset($settings['password'])
      ? $settings['password']
      : NULL,
  );

  return $form;
}

/**
 * Implements CALLBACK_commerce_payment_method_redirect_form().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 * @param $order
 *   The order.
 * @param $payment_method
 *   The payment method.
 */
function commerce_payline_web_page_redirect_form($form, &$form_state, $order, $payment_method) {
  libraries_load('payline');

  $merchant_id = isset($payment_method['settings']['merchant_id'])
    ? $payment_method['settings']['merchant_id']
    : '';
  $access_key = isset($payment_method['settings']['access_key'])
    ? $payment_method['settings']['access_key']
    : '';
  $host = isset($payment_method['settings']['proxy_host'])
    ? $payment_method['settings']['proxy_host']
    : '';
  $port = isset($payment_method['settings']['proxy_port'])
    ? $payment_method['settings']['proxy_port']
    : '';
  $login = isset($payment_method['settings']['proxy_login'])
    ? $payment_method['settings']['proxy_login']
    : '';
  $password = isset($payment_method['settings']['proxy_password'])
    ? $payment_method['settings']['proxy_password']
    : '';
  $environment = isset($payment_method['settings']['environment'])
    ? $payment_method['settings']['environment']
    : '';

  $paylineSDK = new paylineSDK($merchant_id, $access_key, $host, $port, $login, $password, $environment);

  $params = array(
    'order' => array(
      'ref' => $order->order_id,
      'amount' => 100,
      'currency' => 978,
      'date' => date('d/m/Y H:i', $order->changed)
    ),
    'payment' => array(
      'mode' => 'CPT',
      'action' => 100,
      'contractNumber' => 1234567,
      'amount' => 100,
      'currency' => 978,
    ),
    'cancelURL' => url('cart', array(
      'absolute' => TRUE,
    )),
    'returnURL' => url('checkout/' . $order->order_id . '/complete', array(
      'absolute' => TRUE,
    )),
  );

  $webPayment = $paylineSDK->doWebPayment($params);

  $form['#action'] = $webPayment['redirectURL'];

  return $form;
}