# Installation

1. Download payline SDK and WSDL from https://payline.atlassian.net/wiki/spaces/DT/pages/54001918/Kit+int+gration+PHP
2. Create a directory sites/all/libraries/payline
3. Put the payline SDK and WSDL into this directory
4. Rename SDK file so you have sites/all/libraries/payline/paylineSDK.php
5. Rename WSDL file so you have sites/all/libraries/payline/v4.44.1.wsdl